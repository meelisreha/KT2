
package ee.bcs.valiit.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import ee.bcs.valiit.model.Geen;
import ee.bcs.valiit.model.MessageDTO;
import ee.bcs.valiit.model.User;
import ee.bcs.valiit.services.AuthenticationService;
//import ee.bcs.valiit.services.CompanyService;
import ee.bcs.valiit.services.GeenService;
import ee.bcs.valiit.services.WorkaholicService;

@Path("/")
public class RestResource {


    @GET
    @Path("/get_geen")
    @Produces(MediaType.TEXT_PLAIN)
    public Geen getGeen(){
        return GeenService.getGeen();
    }

    @POST
    @Path("/addgeentodb")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String addGeen(Geen geen) {
        GeenService.addGeen(geen);
        return "OK add Geen";
    }

}

    /*
	@GET
	@Path("/hi/text")
	@Produces(MediaType.TEXT_PLAIN)
	public String getHi(@DefaultValue("World") @QueryParam("name") String name) {
		return String.format("Hello, %s!", name);
	}

	@GET
	@Path("/hi/json/map/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getHi2(@PathParam(value = "name") String name) {
		Map<String, String> response = new HashMap<>();
		response.put("greeting", String.format("Hello, %s!", name));
		return Response.ok(response).build();
	}

	@GET
	@Path("/hi/json/dto")
	@Produces(MediaType.APPLICATION_JSON)
	public MessageDTO getHi3(@DefaultValue("World") @QueryParam("name") String name) {
		MessageDTO message = new MessageDTO();
		message.setText("Hello, " + name + "!!!");
		return message;
	}

	@POST
	@Path("/message")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String message(MessageDTO message) {
		return "Sõnum oli järgmine: " + message.getText();
	}

	@POST
	@Path("/addnumbers")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addNumbers(@FormParam("number1") String number1, @FormParam("number2") String number2) {
		int value1 = Integer.parseInt(number1);
		int value2 = Integer.parseInt(number2);
		int result = value1 + value2;
		Map<String, String> response = new HashMap<>();
		response.put("result", String.valueOf(result));
		response.put("comment", "See summa arvutati kokku RestResource klassis serveri pool.");
		return Response.ok(response).build();
	}

	@GET
	@Path("/message/db")
	@Produces(MediaType.TEXT_PLAIN)
	public String getTextFromDb() throws SQLException, ClassNotFoundException {
		// Class.forName("com.mysql.cj.jdbc.Driver");
		Class.forName("org.mariadb.jdbc.Driver");
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/valiit", "root", "tere")) {
			try (Statement stmt = conn.createStatement()) {
				try (ResultSet rs = stmt.executeQuery("SELECT simple_column FROM simpledemo")) {
					rs.first();
					String result = rs.getString(1);
					conn.close();
					return result;
				}
			}
		}
	}

/*
	@GET
	@Path("/get_companies")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Company> getCompanies(@Context HttpServletRequest req) {
		if (isUserAuthorized(req, "Admin") || isUserAuthorized(req, "User")) { 
			return CompanyService.getCompanies();
		} else {
			return new ArrayList<>();
		}
	}

	@GET
	@Path("/get_company")
	@Produces(MediaType.APPLICATION_JSON)
	public Company getCompany(@QueryParam("company_id") int companyId) {
		return CompanyService.getCompany(companyId);		
	}

	@POST
	@Path("/add_company")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String addCompany(Company company) {
		CompanyService.addCompany(company);
		return "OK";
	}

	@POST
	@Path("/modify_company")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String modifyCompany(Company company) {
		CompanyService.modifyCompany(company);
		return "OK";
	}

	@POST
	@Path("/delete_company")
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteCompany(@FormParam("company_id") int companyId) {
		CompanyService.deleteCompany(companyId);
		return "OK";
	}*//*


	@GET
	@Path("/is_working_allowed")
	@Produces(MediaType.TEXT_PLAIN)
	public String isWorkingAllowed(@QueryParam("test_date") String testDate) throws UnsupportedEncodingException {
		return WorkaholicService.isWorkingAllowed(URLDecoder.decode(testDate, "UTF-8"));
	}

	@GET
	@Path("/set_cookie")
	@Produces(MediaType.TEXT_PLAIN)
	public Response setCookie() {
		NewCookie myCookie = new NewCookie("MY_TEST_COOKIE", String.valueOf(Math.random()), "/", null, null, -1, false,
				false);

		return Response.ok("Cookie set successfully!").cookie(myCookie).build();
	}

	@GET
	@Path("/read_cookie")
	@Produces(MediaType.TEXT_PLAIN)
	public String readCookie(@CookieParam("MY_TEST_COOKIE") String myCookie) {
		return "The cookie value was " + myCookie;
	}

	@GET
	@Path("/set_session_info")
	@Produces(MediaType.TEXT_PLAIN)
	public String setSessionInfo(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		String myTestAttr = (String) session.getAttribute("TEST");
		if (myTestAttr != null) {
			return "Session attribute found: " + myTestAttr;
		} else {
			// Session attribute was not found.
			session.setAttribute("TEST", String.valueOf(Math.random()));
			return "Session attribute generated: " + (String) session.getAttribute("TEST");
		}
	}

	@POST
	@Path("/authenticate_user")
	@Produces(MediaType.TEXT_PLAIN)
	public String authenticateUser(@Context HttpServletRequest req, @FormParam("email") String email,
			@FormParam("password") String password) {
		User user = AuthenticationService.getUser(email, password);
		if (user == null) {
			// Autentimine ebaõnnestus.
			return "FAIL";
		} else {
			// Autentimine õnnestus.
			HttpSession session = req.getSession(true);
			session.setAttribute("AUTH_USER", user);
			return "SUCCESS";
		}
	}

	@GET
	@Path("/get_authenticated_user")
	@Produces(MediaType.APPLICATION_JSON)
	public User getAuthenticatedUser(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("AUTH_USER") != null) {
			// Kasutaja on sisse loginud.
			return (User)session.getAttribute("AUTH_USER");
		} else {
			// Kasutaja ei ole sisse loginud.
			return new User(); // Kasutame tühja objekti.
		}
	}
	
	@GET
	@Path("/logout")
	@Produces(MediaType.TEXT_PLAIN)
	public String logout(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		session.removeAttribute("AUTH_USER");
		return "SUCCESS";
	}
	
	private boolean isUserAuthorized(@Context HttpServletRequest req, String expectedRole) {
		HttpSession session = req.getSession(true);
		User user = null;
		if (session.getAttribute("AUTH_USER") != null) {
			// Kasutaja on sisse loginud.
			user = (User)session.getAttribute("AUTH_USER");
			return user.getRole().equals(expectedRole);
		}
		return false;
	}
}

*/