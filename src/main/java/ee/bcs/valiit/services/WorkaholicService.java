package ee.bcs.valiit.services;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class WorkaholicService {

	public static String isWorkingAllowed(String testDate) {
		Calendar myCalendar = Calendar.getInstance();
		Map<String, Integer> myDateParts = deriveDateParts(testDate);
		myCalendar.set(myDateParts.get("year"), myDateParts.get("month"), myDateParts.get("day"),
				myDateParts.get("hour"), myDateParts.get("minute"));
		
		// Mitmes nädalapäev?
		int dayOfWeek = myCalendar.get(Calendar.DAY_OF_WEEK);
		if (dayOfWeek >= 3 && dayOfWeek <= 5) { // Teisipäev - neljapäev
			return "YES";
		} else if (dayOfWeek == 7 || dayOfWeek == 1) { // Laupäev, pühapäev
			return "NO";
		} else if (dayOfWeek == 2) { // Esmaspäev
			if (myDateParts.get("hour") >= 6) {
				return "YES";
			} else {
				return "NO";
			}
		} else { // Reede
			if (myDateParts.get("hour") < 16) {
				return "YES";
			} else {
				return "NO";
			}
		}
	}

	// 2018-05-04 09:11 -> lugeda välja aasta, kuu, päev, tund, minut
	private static Map<String, Integer> deriveDateParts(String date) {
		Map<String, Integer> dateParts = new HashMap<>();
		String[] dateAndTime = date.split(" ");
		String[] dayComponents = dateAndTime[0].split("-");
		String[] timeComponents = dateAndTime[1].split(":");

		dateParts.put("year", Integer.parseInt(dayComponents[0]));
		dateParts.put("month", Integer.parseInt(dayComponents[1]) - 1);
		dateParts.put("day", Integer.parseInt(dayComponents[2]));
		dateParts.put("hour", Integer.parseInt(timeComponents[0]));
		dateParts.put("minute", Integer.parseInt(timeComponents[1]));

		return dateParts;
	}

}
