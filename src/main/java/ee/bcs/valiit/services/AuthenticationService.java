package ee.bcs.valiit.services;

import java.sql.ResultSet;
import java.sql.SQLException;

import ee.bcs.valiit.model.User;

public class AuthenticationService {

	/*public static User getUser(String email, String password) {
		String sql = String.format("SELECT * FROM user WHERE email = '%s' AND password = '%s'", email, password);
		ResultSet userRecord = CompanyService.executeSql(sql);
		try {
			if (userRecord != null && userRecord.next()) {
				return instantiateUser(userRecord);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}*/

	protected static User instantiateUser(ResultSet userRecord) throws SQLException {
		User user = new User();
		user.setId(userRecord.getInt("id"));
		user.setFirstName(userRecord.getString("first_name"));
		user.setLastName(userRecord.getString("last_name"));
		user.setEmail(userRecord.getString("email"));
		user.setRole(userRecord.getString("role"));
		return user;
	}

}
