package ee.bcs.valiit.model;

import java.util.Random;

public class Geen {

    private Alleel alleel1; // Alleel name ja boolean
    private Alleel alleel2;


    public boolean checkReesus(Alleel alleel1, Alleel alleel2) {
        if (alleel1.isReesus() == true || alleel2.isReesus() == true) {
            return  true;
        } else {
            return false;
        }
    }

    public Geen(Alleel alleel1, Alleel alleel2) {
        this.alleel1 = alleel1;
        this.alleel2 = alleel2;
    }


    public Alleel randomAlleel() {
        Random rnd = new Random();
        if (rnd.nextBoolean()) {    // kui random on üle 0,5 on nextBoolean true ja väljastab alleel1, kui random on alla 0,5 on nextBoolean 0 ja väljasta alleel2
            return alleel1;
        } else {
            return alleel2;
        }
    }

    public static String newGene() {
        Alleel alleel1 = new Alleel("reesus positiivne", true);
        Alleel alleel2 = new Alleel("reesus negatiivne", false);

        Geen mother = new Geen(alleel1, alleel2);
        Geen father = new Geen(alleel1, alleel2);
        Alleel motherRandomAlleel = mother.randomAlleel();
        Alleel fatherRandomAlleel = father.randomAlleel();
        Geen newGeen = new Geen(motherRandomAlleel, fatherRandomAlleel);

        return "New gene was created: with name reesus and geen being reesus: " + newGeen.checkReesus(motherRandomAlleel, fatherRandomAlleel) + "\n alleel1: " + motherRandomAlleel.isReesus() + "\n alleel2: " + fatherRandomAlleel.isReesus();
    }


    @Override
    public String toString() {
        return String.format("%s, %s" , alleel1, alleel2);

    }
}
