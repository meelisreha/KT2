package ee.bcs.valiit.model;

public class Alleel {

    protected String name;

    protected boolean reesus;


    // Alleeli builder kaheparameetriga. Alleel alleel1 = new Alleel(nimeke, true);
    public Alleel(String name, boolean reesus) {
        this.name = name;
        this.reesus = reesus;
    }

    public boolean isReesus() {
        return reesus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setReesus(boolean reesus) {
        this.reesus = reesus;
    }


    @Override
    public String toString() {
        return String.format("%s, %s", name, reesus);
    }
}


/*
 *       Alleel alleel1 = new Alleel();
 *       alleel1.setName("lilleke")
 *       alleel1.setReesus("true")


 *       Alleel alleel1 = new Alleel("lilleke", true);
 *
 *
 *
 *
 * */